resource "aws_subnet" "dev-subnet-1" {
  vpc_id = aws_vpc.development-vpc.id
  cidr_block = var.var_subnet
  availability_zone = var.availability_zone
  tags = {
    Name : "dev-subnet"
  }
}


resource "aws_route_table" "route_table" {
vpc_id =var.vpc_id
route{
cidr_block ="0.0.0.0/0"
gateway_id =aws_internet_gateway.aws_gateway.id
}
tags ={
  Name: "aws_route_table_yash"
}
}


resource "aws_internet_gateway" "aws_gateway" {
vpc_id=var.vpc_id
tags ={
  Name: "aws_gateway_yash"
}
}

resource "aws_route_table_association" "rt_subnet" {
  subnet_id = aws_subnet.dev-subnet-1.id
  route_table_id = aws_route_table.route_table.id
}

